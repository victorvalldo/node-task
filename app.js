const express = require('express')
const app = express()
const port = 3000

/**
 * The function returns the product of p1 and p2
 */

function myFunction(p1, p2) {
  return p1 * p2;   
}

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})